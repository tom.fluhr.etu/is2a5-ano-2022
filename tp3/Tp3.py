

import autograd.numpy as np
import matplotlib.pyplot as plt
import autograd as ag


#Q1 

def f (a,b):
    return a**3 + 2*a**2 - 2*a*b + b**2 + a*b**3 - 2*b +5

fig = plt.figure(figsize = (20,20))
ax = plt.axes(projection='3d')

ax.set_xlabel('$a$', labelpad=20)
ax.set_ylabel('$b$', labelpad=20)
ax.set_zlabel('$f(a,b)$', labelpad=20)

xplot = np.arange (-2, 1, 0.05)
yplot = np.arange (-2, 2, 0.05)

X, Y = np.meshgrid (xplot, yplot)
Z = f(X,Y)

zmin = 3
zmax = 14

m, n = Z.shape
for i in range (m) :
    for j in range (n) :
        Z[i,j] = max(min(Z[i,j], zmax), zmin)

ax.plot_surface(X, Y, Z, cmap="spring_r", lw=0.5, rstride=1, cstride=1, alpha=0.5)
ax.contour(X, Y, Z, 30, colors="k", linestyles="dotted")

plt.show ()

print("avec cette estimation graphique nous obtenons 5 point : ")
print("X1=(0.25;1)")
print("X2=(-1.5;0.7)")
print("X3=(-1.2;0.2)")
print("X4=(-0.3;1.6)")
print("X5=(0.7;2.2)")

#Q2 

def nabla_f (a,b):
    return np.array ([3*a**2+4*a-2*b+b**3,-2*a+2*b+3*a*b**2-2], dtype=np.float64)

def H_f (a,b):
    return np.array ([[6*a+4,-2+3*b**2],
                      [-2+3*b**2,2*a+6*a*b]], dtype=np.float64)

#Q3
print("Q3 pour le point X2")
u=np.array([-1.5, 0.7],dtype=np.float64)
for i in range (10) :
    a=u[0]
    b=u[1]
    MJ=-H_f(a, b)
    F=nabla_f (a,b)
    h=np.linalg.solve(MJ,F)
    u=u + h
    print(u)
    
#0.25 1 --> 0.22 0.96
#-1.5 0.7 --> -1.56 0.75
#-1.2 0.2 --> Trouve celui du haut 
#-0.3 1.6  --> -0.23 1.56
#0.7 -2.2 --> 0.62 -1.96
print("Nous obtennons pour les autres points :")
print("X1=(0.22 0.96)")
print("X2=(-1.56 0.75)")
print("X4=(-0.23 1.56)")
print("X5=(0.62 -1.96)")



#Q4 
print("Nous regardons ici le type de point que c'est")
x=np.array ([2,3], dtype=np.float64)
res=np.linalg.eigvalsh(H_f(0.22, 0.96))
print(res)
print("X1 a ses VP > 0 --> Min locale")
res=np.linalg.eigvalsh(H_f(-1.56, 0.75))
print(res)
# 2 VP neg donc maximum locale
print("X2 a ses VP < 0 --> Maximum locale")
print("X3 a une VP > 0 et une vp < 0 --> C'est un col")
res=np.linalg.eigvalsh(H_f(-0.23, 1.56))
print(res)
print("X4 a une VP > 0 et une vp < 0 --> C'est un col")
res=np.linalg.eigvalsh(H_f(0.62, -1.96))
print(res)
print("X5 a une VP > 0 et une vp < 0 --> C'est un col")



#Q5 

print("Verification des resultat par autograd pour X1")
def F (u) :
    a = u[0]
    b = u[1]
    return f(a,b)


H2=ag.hessian(F) 
G2=ag.grad(F)

u=np.array([0.25,1],dtype=np.float64)

for i in range (5) :
    G=G2(u)
    H=H2(u)
    h=np.linalg.solve(-H,G)
    u=u + h
    print(u)
    
print("Nous obtenons bien le même résultat")
#Q6

#BackWard --> efficace pour R --> R^p si m>p SINON Forward 

def f (a,b):
    return a**3 + 2*a**2 - 2*a*b + b**2 + a*b**3 - 2*b +5

#Rm -> Rp si m>p backward si m<p forward

def fter(u):
    a=u[0]
    b=u[1]
    t1=b**2
    t2=a**2
    t3=a*t1*b - 2 * a * b +t2*a - 2*b + t1 + 2*t2 +5
    return t3

def nabla_fter(u):
    a=u[0]
    b=u[1]
    t1=b**2
    t2=a**2
    t3=a*t1*b - 2 * a * b +t2*a - 2*b + t1 + 2*t2 +5
    df_dt3=1
    df_dt2= (a+2)*df_dt3
    df_dt1=(a*b +1)*df_dt3
    df_da=2*a*df_dt2 + (t1*b - 2*b +t2)*df_dt3
    df_db=2*b*df_dt1 +(a*t1 -2*a -2)*df_dt3
    return np.array([df_da, df_db])



nabla_fter(u)
