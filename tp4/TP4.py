import autograd.numpy as np
import matplotlib.pyplot as plt
import autograd as ag


#Q1

print(0.2255**2+0.9312**2<=0.5) #--> Ne satisfait pas la contrainte

#On vois que le point le plus bas appertenant au cercle est un point en bas du cercle autour de (0.15 0.6)
# C(a,b)=0 --> a² + b² - 0.5 = 0

#Q2 et 3

def f (a,b):
    return a**3 + 2*a**2 - 2*a*b + b**2 + a*b**3 - 2*b + 5


def nabla_f (a,b) :
    return np.array ([b**3 + 3*a**2 + 4*a - 2*b, 
                      3*a*b**2 - 2*a + 2*b - 2], dtype=np.float64)

def nabla_f2 (a,b) :
    return np.array ([2*a,
                      2*b], dtype=np.float64)

fig = plt.figure(figsize = (20,20))
ax = plt.axes(projection='3d')

ax.set_xlabel('$a$', labelpad=20)
ax.set_ylabel('$b$', labelpad=20)
ax.set_zlabel('$f(a,b)$', labelpad=20)

xplot = np.arange (-.8, 1.2, 0.05)
yplot = np.arange (-.8, 1.2, 0.05)

# Tracé du graphe de f
X, Y = np.meshgrid (xplot, yplot)
Z = f(X,Y)

m, n = Z.shape
for i in range (m) :
    for j in range (n) :
        Z[i,j] = max(min(Z[i,j], 12), 3)

ax.plot_surface(X, Y, Z, cmap="spring_r", lw=0.5, rstride=1, cstride=1, alpha=0.5)
ax.contour(X, Y, Z, 30, colors="k", linestyles="dotted")

# Le minimim local de f sans tenir compte de la contrainte
zero = np.array ([0.2255014396, 0.9318083312])
ax.scatter ([zero[0]], [zero[1]], [f(zero[0],zero[1])], color='black')

# Tracé de la contrainte a**2 + b**2 = r**2 (avec r**2 = 1/2)

r = 1/np.sqrt(2)
n = 100
angles = np.linspace (-np.pi, np.pi, n)
cxplot = np.array ([r * np.cos(theta) for theta in angles])
cyplot = np.array ([r * np.sin(theta) for theta in angles])
czplot = np.array ([f (cxplot[i], cyplot[i]) for i in range (0,n)])
ax.plot (cxplot, cyplot, czplot, color='black')

# Tracé d'un gradient (longueur normalisée)

a, b = 0.2, 0.7
grad_f = nabla_f (a, b)
grad_f = (.25/np.linalg.norm(grad_f,2)) * grad_f
ax.quiver (a, b, f(a,b), grad_f[0], grad_f[1], 0, color='blue')

a, b = 0.20, 0.7
grad_f2 = nabla_f2 (a, b)
grad_f2 = (.25/np.linalg.norm(grad_f2,2)) * grad_f2
ax.quiver (a, b, f(a,b), grad_f2[0], grad_f2[1], 0, color='red')


a, b = 0.5, -0.5
grad_f = nabla_f (a, b)
grad_f = (.25/np.linalg.norm(grad_f,2)) * grad_f
ax.quiver (a, b, f(a,b), grad_f[0], grad_f[1], 0, color='blue')

a, b = 0.6, -0.4
grad_f2 = nabla_f2 (a, b)
grad_f2 = (.25/np.linalg.norm(grad_f2,2)) * grad_f2
ax.quiver (a, b, f(a,b), grad_f2[0], grad_f2[1], 0, color='red')

ax.text (a, b, f(a,b), 'un gradient')
plt.show ()

# Le minimum semble etre vers 0.2 0.7 et le max autour de 0.6 -0.4


#Q4 

#Lagrangien --> L(a,b,LDA) = f(a,b) + LDAc*c(a,b) = a**3 + 2*a**2 - 2*a*b + b**2 + a*b**3 - 2*b + 5 + LDA *(a**2 + b**2 - 0.5)


#Q5 

def Lagrangien (u) :
    a = u[0]
    b= u [1]
    LDA = u[2]
    return f(a,b) + LDA *(a**2 + b**2 - 0.5)

#Q6 

H_Lagrangien=ag.hessian(Lagrangien) 
nabla_Lagrangien=ag.grad(Lagrangien)

#Q7

u=np.array([0.25,1,10],dtype=np.float64)

for i in range (5) :
    G=nabla_Lagrangien(u)
    H=H_Lagrangien(u)
    h=np.linalg.solve(-H,G)
    u=u + h
    print(u)
    
#Nous obtenons [a=0.18529823 b=0.6823962  LDA=0.54729467]

#Q8
def backtracking_line_search (un, alpha0, h) :
    rho = .5
    c = .5
    alpha = alpha0
    #a = un[0]
    #b = un[1]
    #LDA = un [2]
    D_h = np.dot (nabla_Lagrangien (u), h)
    while Lagrangien(u) > Lagrangien(u) + c * alpha * D_h :
        alpha *= rho
    return alpha

u=np.array([0.25,0.25,1],dtype=np.float64)

for i in range (5) :
    G=nabla_Lagrangien(u)
    H=H_Lagrangien(u)
    h=np.linalg.solve(-H,G)
    alpha = backtracking_line_search (u, 1., h)
    print(alpha)
    u = u + alpha*h
    print(u)
    
#Nous obtenons [a=0.18529824 b=0.6823962  LDA=0.54729459]

#Q9

u=np.array([0.25,0.25,1],dtype=np.float64)

for i in range (5) :
    h=nabla_Lagrangien(u)
    alpha = backtracking_line_search (u, 1., h)
    print(alpha)
    u = u + alpha*h
    print(u)
    
#Nous trouvons pas du tout le resultat -->> 


#Q10

def decompose (u, v) :
    if(len(u)==3):
        return u[0], u[1], u[2]
    if(len(u)==2):
        return v[0], u[0], u[1]
    else:
        return u[0], v[0], v[1]

def trace_cercle(u,v):
    r = decompose (u, v)[0]
    X1 = decompose (u, v)[1]
    X2 = decompose (u, v)[2]
    n = 100
    angles = np.linspace (-np.pi, np.pi, n)
    cxplot = np.array ([X1 +r * np.cos(theta) for theta in angles])
    cyplot = np.array ([X2+ r * np.sin(theta) for theta in angles])
    fig, ax = plt.subplots(1)
    
    ax.plot (cxplot, cyplot, color='red')
    ax.set_aspect(1)
    
    plt.grid(linestyle='--')
    plt.show()


u, v = [2, 1], [1]
trace_cercle(u,v)

Tx = np.array ([11,12,8,7,3], dtype=np.float64)
Ty = np.array ([4,8,13,12,7], dtype=np.float64)
u, v = [8, 7], [4]

def f(u,v,Tx,Ty):
    r=decompose(u,v)[0]
    xc=decompose(u,v)[1]
    yc=decompose(u,v)[2]
    somme=0
    for i in range(0,len(Tx)-1):
        angle=np.arctan2(Tx[i],Ty[i])*180/np.pi
        somme+=(r/np.tan(angle))**2
    return somme

H2=ag.hessian(f) 
G2=ag.grad(f)

u=np.array([u])

for i in range (5) :
    G=G2(u)
    H=H2(u)
    h=np.linalg.solve(-H,G)
    u=u + h
    print(u)

#Q17

Ty = np.array([.53 ,.53 ,1.53 ,2.53 ,12.53 ,21.53 ,24.53 ,28.53 ,28.53 ,30.53])
Tx=np.array([i for i in range (0,Ty.shape[0])])


def sigmoide(x,u):
    kappa= u[0]
    alpha = u[1]
    rho = u[2]
    return kappa / (1 + np.exp(alpha-rho*x))

def f(u):
    sum =0
    for i in range(0,Ty.shape[0]):
        sum = sum + (Ty[i]-sigmoide(Tx[i],u))**2
    return sum


H2=ag.hessian(f) 
G2=ag.grad(f)

u=np.array([30.54,5.163,1.188])
for i in range (5) :
    G=G2(u)
    H=H2(u)
    h=np.linalg.solve(-H,G)
    u=u + h
    print(u)