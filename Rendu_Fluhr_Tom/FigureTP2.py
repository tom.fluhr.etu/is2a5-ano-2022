#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 23 10:23:19 2022

@author: tfluhr
"""

import autograd.numpy as np
import matplotlib.pyplot as plt
import autograd as ag


#Q1 

def f (a,b):
    return a**3*b- 3*a**2*(b-1) +b**2-1

def g (a,b):
    return a**2*b**2 - 2

fig = plt.figure(figsize = (20,20))
ax = fig.add_subplot(1, 1, 1, projection='3d')

ax.set_xlabel('$a$', labelpad=20)
ax.set_ylabel('$b$', labelpad=20)
ax.set_zlabel('$f(a,b)$', labelpad=20)

xplot = np.arange (-3.5, 0, 0.1)
yplot = np.arange (0, 2,0.1)

##### Graphe de f #####
X, Y = np.meshgrid (xplot, yplot)
Z = f(X,Y)

ax.plot_surface(X, Y, Z, cmap="spring_r", lw=0.5, rstride=1, cstride=1, alpha=0.5)
ax.contour(X, Y, Z, 10, colors="k", linestyles="dashed")
ax.contour(X, Y, Z, 1, colors="black",  levels=np.array([0], dtype=np.float64), linestyles="solid")

##### Graphe de g #####
X, Y = np.meshgrid (xplot, yplot)
Z = g(X,Y)

ax.plot_surface(X, Y, Z, cmap="autumn_r", lw=0.5, rstride=1, cstride=1, alpha=0.5)
ax.contour(X, Y, Z, 10, colors="k", linestyles="dashed")
ax.contour(X, Y, Z, 1, colors="black",  levels=np.array([0], dtype=np.float64), linestyles="solid")

plt.show()
