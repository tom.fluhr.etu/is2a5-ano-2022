#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 16 09:46:49 2022

@author: tfluhr
"""
import numpy as np
import matplotlib.pyplot as plt

#On cherche racine cubique de 2 --> nOMBRE X TEL QUE x AU CUBE = 2
def f(x):
    return x**3 - 2

def fp(x):
    return 3*x**2

print("approximation de la racine cubique de 2 par la méthode de newtown")
U0 = 2
for i in range(6):
    print(U0)
    u1 = U0 - f(U0)/fp(U0)
    U0=u1

print("Vérification que U0 au cube vaut bien 2")
print(U0**3,'\n')

#Calucl minimum
alpha,beta,gamma,mu=np.array([ 1.587, -3.1447, -0.3747, 7.484])

def f(x) :
    return alpha*x**3 + beta*x**2 + gamma*x + mu

#trouver un minimum de F c'est trouver la dérivé = 0 --> Newton sur Fprime nous sortira un extremum 
    
def fprime(x) :
    return alpha*3*x**2 + beta*2*x+ gamma

def fseconde(x) :
    return alpha*6*x + beta*2

print("recherhe du minimum sur la cubique de la partie 1")
U0 = 3
for i in range(6):
    print(U0)
    u1 = U0 - fprime(U0)/fseconde(U0)
    U0=u1
    
print('\n fseconde =',fseconde(U0),'La dérivé seconde est positif on a donc bien un minimum locale')

