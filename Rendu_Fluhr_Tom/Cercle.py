import autograd.numpy as np
import matplotlib.pyplot as plt
import autograd as ag



def decompose (u, v) :
    if(len(u)==3):
        return u[0], u[1], u[2]
    if(len(u)==2):
        return v[0], u[0], u[1]
    else:
        return u[0], v[0], v[1]

def trace_cercle(u,v):
    r = decompose (u, v)[0]
    X1 = decompose (u, v)[1]
    X2 = decompose (u, v)[2]
    n = 100
    angles = np.linspace (-np.pi, np.pi, n)
    cxplot = np.array ([X1 +r * np.cos(theta) for theta in angles])
    cyplot = np.array ([X2+ r * np.sin(theta) for theta in angles])
    fig, ax = plt.subplots(1)
    
    ax.plot (cxplot, cyplot, color='red')
    ax.set_aspect(1)
    
    plt.grid(linestyle='--')
    plt.show()


u, v = [2, 1], [1]
trace_cercle(u,v)
