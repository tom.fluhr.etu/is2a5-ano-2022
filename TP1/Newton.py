#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 16 09:46:49 2022

@author: tfluhr
"""
import numpy as np

#On cherche racine cubique de 2 --> nOMBRE X TEL QUE x AU CUBE = 2
def f(x):
    return x**3 - 2

def fp(x):
    return 3*x**2

U0 = 2
for i in range(6):
    print(U0)
    u1 = U0 - f(U0)/fp(U0)
    U0=u1

print(U0**3)

#Calucl minimum
alpha,beta,gamma,mu=np.array([ 0.62665828, -2.53773594, 11.54283316, 13.46909209])

def f(x) :
    return alpha*x**3 + beta*x**2 + gamma*x + mu

#trouver un minimum de F c'est trouver la dérivé = 0 --> Newton sur Fprime nous sortira un extremum 
    
def fprime(x) :
    return alpha*3*x**2 + beta*2*x+ gamma

def fseconde(x) :
    return alpha*6*x + beta*2

print("minimum")
U0 = 2
for i in range(6):
    print(U0)
    u1 = U0 - fprime(U0)/fseconde(U0)
    U0=u1
    
#Seconde positive --> min sinon max 

