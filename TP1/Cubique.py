import numpy as np
import scipy.linalg as nla
import matplotlib.pyplot as plt

alpha = 0.5
beta = -1
gamma = 6
mu = 3

def f(x) :
    return alpha*x**3 + beta*x**2 + gamma*x + mu


Tx = np.array ([2,-1,4,3,5,7], dtype=np.float64)
Tyc = np.array ([f(x) for x in Tx])
#print(Tye)
Tye = Tyc + 20*np.random.random (6) 
#print(Tyc)

erreurinitial = nla.norm (Tyc - Tye, 2)
#print("l'erreur initial est " + str(erreurinitial))

C1= Tx**3
C2= Tx**2
C3= Tx
C4= np.ones((6,1),float)
A=np.column_stack((C1,C2,C3,C4))
#print(A)


Q1, R1 = nla.qr(A, mode='economic') 
Q1Tb = np.dot (np.transpose (Q1), Tye) #Produit matriciel

ParamCalc=(nla.solve_triangular (R1, Q1Tb, lower=False))
#print(ParamCalc)

Tycalc =np.dot(A,ParamCalc)
#print(Tycalc)
erreurcalcul = nla.norm (Tye- Tycalc, 2)
print("l'erreur initial est " + str(erreurinitial) + " et l'erreur calculé est " + str(erreurcalcul))


xplot = np.linspace (-1.1, 7.1, 100)
yplot = [f(x) for x in xplot]
plt.plot (xplot, yplot)
plt.scatter (Tx, Tye)
plt.scatter (Tx, Tycalc)
plt.show ()

#Tye --> point de la vrai courbe
#Tyc --> point experimentaux
#Tycalc --> point calculer
#On essaie de retrouver une courbe a partir des point qu'on a pris de base en "trichant" 
#Point orange est une courbe qui se rapproche des point experimentéaux plus que la 1er







