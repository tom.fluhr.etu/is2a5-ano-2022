#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 23 10:23:19 2022

@author: tfluhr
"""

import autograd.numpy as np
import matplotlib.pyplot as plt
import autograd as ag


#Q1 

def f (a,b):
    return a**3*b- 3*a**2*(b-1) +b**2-1

def g (a,b):
    return a**2*b**2 - 2

fig = plt.figure(figsize = (20,20))
ax = fig.add_subplot(1, 1, 1, projection='3d')

ax.set_xlabel('$a$', labelpad=20)
ax.set_ylabel('$b$', labelpad=20)
ax.set_zlabel('$f(a,b)$', labelpad=20)

xplot = np.arange (-3.5, 0, 0.1)
yplot = np.arange (0, 2,0.1)

##### Graphe de f #####
X, Y = np.meshgrid (xplot, yplot)
Z = f(X,Y)

ax.plot_surface(X, Y, Z, cmap="spring_r", lw=0.5, rstride=1, cstride=1, alpha=0.5)
ax.contour(X, Y, Z, 10, colors="k", linestyles="dashed")
ax.contour(X, Y, Z, 1, colors="black",  levels=np.array([0], dtype=np.float64), linestyles="solid")

##### Graphe de g #####
X, Y = np.meshgrid (xplot, yplot)
Z = g(X,Y)

ax.plot_surface(X, Y, Z, cmap="autumn_r", lw=0.5, rstride=1, cstride=1, alpha=0.5)
ax.contour(X, Y, Z, 10, colors="k", linestyles="dashed")
ax.contour(X, Y, Z, 1, colors="black",  levels=np.array([0], dtype=np.float64), linestyles="solid")

plt.show()

print("Nous obtenons deux point, un premier point avec comme coordonée ((-0,8 ;1,8) et un deuxieme point autour de (-3:0,6)")

def Jac_F (a,b):
    return np.array ([[3*a**2*b-6*a*(b-1), a**3-3*a**2+2*b],
                     [2*a*b**2,2*a**2*b]], dtype=np.float64)
    
#Q3
print("resultat de la Q3 pour l'estimation du premier point")
u=np.array([-0.8,1.8],dtype=np.float64)
for i in range (5) :
    a=u[0]
    b=u[1]
    MJ=-Jac_F(a, b)
    F=np.array ([f(a,b),g(a,b)], dtype=np.float64)
    h=np.linalg.solve(MJ,F)
    u=u + h
    print(u)

#-0.77,1.82
#-2.84 0.49
print("pour le 2eme point nous obtenons -2.84 et 0.49")

#Q4

print("Resultat avec la méthode d'autograd directement pour l'estimation du premier point")
def F (u) :
    a = u[0]
    b = u[1]
    return np.array ([f(a,b),g(a,b)], dtype=np.float64)


JAC2=ag.jacobian(F) #Prend une fonction et retourne une fonction

u=np.array([-0.8,1.8],dtype=np.float64)

for i in range (5) :
    F2=F(u)
    MJ=JAC2(u)
    h=np.linalg.solve(-MJ,F2)
    u=u + h
    print(u)
    
#Q5

def F2(u):
    a = u[0]
    b = u[1]
   
    t1 = a**2
    t2 = b**2
    t3 = (a*b - 3*b + 3)*t1 + t2 -1
    t4 = t1*t2 - 2
   
    return np.array([t3, t4])

def Jac_Q5(u):
    a = u[0]
    b = u[1]
   
    t1 = a**2
    t2 = b**2
    t3 = (a*b - 3*b + 3)*t1 + t2 -1
    t4 = t1*t2 - 2
   
    dt1_da = 2*a
    dt1_db = 0
    dt2_da = 0
    dt2_db = 2*b
    dt3_da = b*t1 + (a*b - 3*b +3)*dt1_da
    dt3_db = (a - 3)*t1 + (a*b - 3*b +3)*dt1_db
    dt4_da = dt1_da*t2 + t1*dt2_da
    dt4_db = dt1_db*t2 + t1*dt2_db
   
    f_dt_da = dt1_da + dt2_da + dt3_da
    f_dt_db = dt1_db + dt2_db + dt3_db
    g_dt_da = dt1_da + dt2_da + dt4_da
    g_dt_db = dt1_db + dt2_db + dt4_db
   
    return np.array([[f_dt_da, f_dt_db], [g_dt_da, g_dt_db]])

print("resultat de la pour l'estimation du premier point avec la dérivation mode forward")
a0 = -0.8
b0 = 1.8 
u0 = np.array([a0, b0])
for i in range(6):
    print('u[%d] = ' % i, u0)
    h = np.linalg.solve(- Jac_Q5(u0), F2(u0))
    u1 = u0 + h
    u0 = u1

